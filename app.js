/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useRef, useEffect} from 'react';
import {WebView} from 'react-native-webview';
import NetInfo from '@react-native-community/netinfo';
import RNFetchBlob from 'rn-fetch-blob';
import { useHistory } from 'react-router-dom';


import type {Node} from 'react';
import {
  SafeAreaView,PermissionsAndroid,
  ScrollView,
  StatusBar,Linking,BackHandler, Alert,
  StyleSheet,
  Text,Platform,
  Button,
  useColorScheme,
  View,
  ActivityIndicator
} from 'react-native';
import { InAppBrowser } from 'react-native-inappbrowser-reborn'
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

var RNFS = require('react-native-fs');


const App: () => Node = () => {
  console.log('Inside APP const');
  const [Url, setUrl] = useState('https://dev-lings.cs107.force.com/#/mobileView');
  var URL = 'https://dev-lings.cs107.force.com/#/mobileView';
 //var token = null;
  var mobileEmailVar = null;
  var mobilePasswordVar = null;
  var recentObjectURL = null;
  const canGoBack = useRef(false);
  const WebViewRef = useRef(null);
  const getData = async () => {
    try {
      //token = await AsyncStorage.getItem('token');
      mobileEmailVar = await AsyncStorage.getItem('mobileEmail');
      mobilePasswordVar = await AsyncStorage.getItem('mobilePassword');
      recentObjectURL = await AsyncStorage.getItem('recentObjectURL');

      console.log('mobileEmailVar ' + mobileEmailVar);
      console.log('mobilePasswordVar ' + mobilePasswordVar);
      console.log('recentObjectURL ' + recentObjectURL);

      if(mobileEmailVar !== null && mobilePasswordVar !== null) {
        URL = 'https://dev-lings.cs107.force.com/#/de/login/'+mobileEmailVar+'&&&'+mobilePasswordVar;
        setUrl(URL);
      }
    } catch(e) {
      // error reading value
    }
  }
  getData();
  
  const [isnetConnected, setIsnetConnected] = useState(true);


  function setInternetStatus() {
    NetInfo.fetch().then(resp => {
      setIsnetConnected(resp.isConnected);
    });
  }
  //To set Internet Status
  setInternetStatus();
  

  useEffect(() => {
    console.log('useEffect');

    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', HandleBackPressed);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', HandleBackPressed);
      }
    }
  }, []); // INITIALIZE ONLY ONCE

  const HandleBackPressed = () => {
    console.log('inside 82')
    console.log('Inside HandleBackPressed ',canGoBack);
      if (canGoBack.current && WebViewRef.current) {
        console.log('inside 84')

        WebViewRef.current.goBack();
          return true; // PREVENT DEFAULT BEHAVIOUR (EXITING THE APP)
      }
      return false;
  }

   function handleWebViewNavigationStateChange(newNavState){

    console.log('Inside handleWebViewNavigationStateChange ',canGoBack.current);

    //var recentObjectURL = null;

    const getData = async () => {
      try {
        recentObjectURL = await AsyncStorage.getItem('recentObjectURL');
        console.log('recentObjectURL ' + recentObjectURL);
      } catch(e) {
        // error reading value
      }
    }
    canGoBack.current = newNavState.canGoBack;
    const { url } = newNavState;
    console.log(url);
    if (!url) return;
    if(recentObjectURL != null){
      
    }
    if (url.includes('loginToken')) {
      //MYT = AsyncStorage.setItem('token',url.substring(url.length - 32,url.length));  
      const cred = url.substring(url.lastIndexOf('/') + 1);
      if(cred){
        let wholeCred = cred.split('&&&');
        AsyncStorage.setItem('mobileEmail',wholeCred[0]);
        AsyncStorage.setItem('mobilePassword',wholeCred[1]);
      }
      //AsyncStorage.setItem('token',url.substring(url.lastIndexOf('/') + 1));
    } else if (url.includes('logout')){
      //AsyncStorage.removeItem('token');
      AsyncStorage.removeItem('mobileEmail');
      AsyncStorage.removeItem('mobilePassword');
      //&& Platform.OS === 'ios'
    } else if (url.includes('.pdf')  && !url.includes('base64')){
      WebViewRef.current.stopLoading();
      if(Platform.OS === 'ios'){
        downloadPDFiOS(url);
      }else{
        checkPermission(url);
      }
      //Linking.openURL(url);
    } else if (url.includes('base64')){
      WebViewRef.current.postMessage('showSpinner');
      WebViewRef.current.stopLoading();
      checkPermission(url);
    } else if(url.includes('salesforce.com')){ //documentforce.com
      console.log('salesforce.com');
      if(Platform.OS === 'ios'){
        //saveImage(url);
        openInBrowser(url);
      }
    }

    /*else if(url.includes('documentforce.com')){ //documentforce.com
      console.log('documentforce.com');
      if(Platform.OS === 'ios' && recentObjectURL != null){
        setUrl(recentObjectURL);
      }
    }

    else if(url.includes('/meine-gegenstaende/')){
      console.log('Inside the myObject page url');
      //url.substring(url.lastIndexOf('/') + 1)
      AsyncStorage.setItem('recentObjectURL',url);
    }*/


    else if(!url.includes('dev-lings') && !url.includes('staging-lings') && !url.includes('www.lings.ch')){
      console.log('Inside other party url');
      openInBrowser(url);
    }

  };

  const openInBrowser = async (url) => {
     /*try {
      const oldStyle = StatusBar.currentStyle;
      StatusBar.setBarStyle('dark-content');
      await InAppBrowser.open(url);
      if(oldStyle) StatusBar.setBarStyle(oldStyle);
    } catch (error) {
      Alert.alert(error.message);
    }*/

    try {
      if (await InAppBrowser.isAvailable()) {
        const result = await InAppBrowser.open(url, {
          // iOS Properties
          dismissButtonStyle: 'cancel',
          preferredBarTintColor: '#87be1c',
          preferredControlTintColor: 'white',
          readerMode: false,
          animated: true,
          modalPresentationStyle: 'fullScreen',
          modalTransitionStyle: 'coverVertical',
          modalEnabled: true,
          enableBarCollapsing: false,
          // Android Properties
          showTitle: true,
          toolbarColor: '#6200EE',
          secondaryToolbarColor: 'black',
          navigationBarColor: 'black',
          navigationBarDividerColor: 'white',
          enableUrlBarHiding: true,
          enableDefaultShare: true,
          forceCloseOnRedirection: false,
          // Specify full animation resource identifier(package:anim/name)
          // or only resource name(in case of animation bundled with app).
          animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right'
          },
          headers: {
            'my-custom-header': 'my custom header value'
          }
        })
        //if(url.includes('blog.lings.ch')){
          WebViewRef.current.goBack();
        //}
        //Alert.alert(JSON.stringify(result));
        /*Alert.alert(JSON.stringify(result),  [
              { text: "OK", onPress: () => WebViewRef.current.goBack() }
            ]);*/
      }
      else Linking.openURL(url);
    } catch (error) {
      //Alert.alert(error.message);
    }
  };

  const checkPermission = async (url) => {
      
    // Function to check the platform
    // If Platform is Android then check for permissions.

    if (Platform.OS === 'ios') {
      saveAttachment(url);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // Start downloading
          saveAttachment(url);
          console.log('Storage Permission Granted.');
        } else {
          // If permission denied then show alert
          alert('Error','Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log("++++"+err);
      }
    }
  };

  function saveImage(url){
    //var openDocVar = null;
    console.log('SaveImage');
    console.log(url);
    console.log('recentObjectURL = '+recentObjectURL);

    Linking.openURL(url);
    /*//WebViewRef.current.postMessage('showSpinner');
    const dirs = RNFetchBlob.fs.dirs;
    var urlLink = url;
    var path;
    path = dirs.DownloadDir;
    RNFetchBlob
        .config({                
            path: path,
            fileCache: true,
        })
        .fetch('GET', urlLink, {
            //some headers ..
        })
        .then((res) => {
          //WebViewRef.current.postMessage('hideSpinner');
          if (Platform.OS === 'ios' && recentObjectURL != null){
            //RNFetchBlob.ios.openDocument(res.data);
            //alert(' Downloaded successfully to '+path);
            Alert.alert("Downloaded successfully to", path,  [
              { text: "OK", onPress: () => setUrl(recentObjectURL) }
            ]);
            //name.includes('pdf') ? RNFetchBlob.ios.openDocument(res.data) : alert(' Downloaded successfully to '+path);;
          }
        })
        .catch((e) => {
          //WebViewRef.current.postMessage('hideSpinner');
            console.log('Error en el fetch: ', e);
        })
        //WebViewRef.current.goForward();
       //WebViewRef.current.goBack();*/
  }

 function saveAttachment(url){
  WebViewRef.current.postMessage('showSpinner');
   console.log('inside saveattachment');
   var split = url.split("/base64,"); //base64Image is my image base64 string
   var name = split[0].split('/');
   var urlLink = split[1];
   name = decodeURIComponent(name.pop());
   
   const dirs = RNFetchBlob.fs.dirs;
   var path;
   if(Platform.OS === 'ios'){
     path = name.includes('pdf') ? dirs.DocumentDir : dirs.PictureDir ;
   }else{
     path = dirs.DownloadDir;
   }
   console.log('urllink '+urlLink , 'name '+name);
   RNFetchBlob
       .config({                
           path: path + '/' + name,
           fileCache: true,
       })
       .fetch('GET', urlLink, {
           //some headers ..
       })
       .then((res) => {
        WebViewRef.current.postMessage('hideSpinner');
         if (Platform.OS === 'ios'){
          name.includes('pdf') ? RNFetchBlob.ios.openDocument(res.data) : alert(' Downloaded successfully to '+path);;
         } else {
          alert('Downloaded successfully to '+path);
         }
       })
       .catch((e) => {
        WebViewRef.current.postMessage('hideSpinner');
           console.log('Error en el fetch: ', e);
       })
        WebViewRef.current.goBack();
       

  // const { fs} = RNFetchBlob;
  // const dirs = RNFetchBlob.fs.dirs;
  // let PictureDir = fs.dirs.PictureDir;
  // let DownloadDir = fs.dirs.DownloadDir;
  // if(name.includes('pdf')){
  //   var path = DownloadDir +'/'+ name;
  // }else{
  //   var path = PictureDir +'/'+ name;
  // }
  // console.log(path)
  // RNFetchBlob.fs.writeFile(path, base64, 'base64')
  // .then((res) => {
  //   alert('Image downloaded successfully to '+path);
  // console.log("File : ", res)}
  
  // )
  // .catch((error) => {
  //   alert(JSON.stringify(error));
  // });
  // 
 }

  function downloadPDFiOS (uri) {
  let uriSplitted = uri.split("/");
  const fileName = uriSplitted.pop();
  const dirs = RNFetchBlob.fs.dirs;
  RNFetchBlob
      .config({                
          path: dirs.DocumentDir + '/' + fileName,
          fileCache: true,
      })
      .fetch('GET', uri, {
          //some headers ..
      })
      .then((res) => {
          RNFetchBlob.ios.openDocument(res.data);
      })
      .catch((e) => {
          console.log('Error en el fetch: ', e);
      })
  }

  if (!isnetConnected) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Button onPress={setInternetStatus()} title="press here to restart" />

        <Text style={{textAlign: 'center'}}>
          Please Check Your Internet Connection
        </Text>
      </View>
    );
  }
  
  return (
      <WebView source={{ uri: Url }} 
      ref={WebViewRef}
      startInLoadingState={false}
      onNavigationStateChange={navState => handleWebViewNavigationStateChange(navState)} 
      style={{ marginTop: 20 }} />

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
